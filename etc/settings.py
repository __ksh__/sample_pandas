import os
import socket


# Root directory
ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Env
if socket.gethostname() == 'hostname':
    DEBUG = True
else:
    DEBUG = False

# Database
DB = 'sqlite:///' + os.path.join(ROOT_DIR, 'bin') + '/db.sqlite3'

# Static files
STATIC_DIR = os.path.join(ROOT_DIR, 'src', 'static')
MEDIA_DIR = os.path.join(ROOT_DIR, 'src', 'media')
TEMPLATE_DIR = os.path.join(ROOT_DIR, 'src', 'templates')
