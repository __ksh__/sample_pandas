import pandas as pd
import pandas_highcharts.core
from sqlalchemy import Column, Integer, Float, Date
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from etc.settings import *

engine = create_engine(DB, echo=True)
Base = declarative_base()


class Temperature(Base):

    """temperture."""

    __tablename__ = 'temperature'

    id = Column(Integer, primary_key=True)
    mesurement_date = Column(Date(), nullable=False)
    highest_temp = Column(Float(precision=0.00), nullable=False)
    minimum_temp = Column(Float(precision=0.00), nullable=False)

    def __init__(self, mesurement_date, highest_temp, minimum_temp):
        """initialize."""
        self.mesurement_date = mesurement_date
        self.highest_temp = highest_temp
        self.minimum_temp = minimum_temp

    def get_data_frame():
        return pd.read_sql(
            "SELECT mesurement_date, highest_temp, minimum_temp \
            FROM temperture LIMIT 1000",
            engine, index_col='mesurement_date')

    @classmethod
    def get_describe(cls):
        df = cls.get_data_frame()
        return df.describe()

    @classmethod
    def get_chart(cls):
        df = cls.get_data_frame()
        print(df)
        chart = pandas_highcharts.core.serialize(
            df,
            kind='bar',
            render_to='my-chart',
            output_type='json',
            title='temperture',
        )
        return chart

Base.metadata.create_all(engine)
