import tornado.ioloop
import tornado.autoreload
from etc.settings import *
from handlers import MainHandler


if __name__ == "__main__":

    # app
    application = tornado.web.Application(
        [
            (r"/", MainHandler),
        ],
        template_path=TEMPLATE_DIR,
        static_path=STATIC_DIR,
        debug=DEBUG
    )

    # http server
    application.listen(8000)
    tornado.autoreload.watch(TEMPLATE_DIR + '/share/base.html')
    ioloop = tornado.ioloop.IOLoop().instance()
    tornado.autoreload.start(ioloop)
    ioloop.start()
