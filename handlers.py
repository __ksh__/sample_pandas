import tornado.web
from etc.settings import *
from models import Temperature


class MainHandler(tornado.web.RequestHandler):

    """view stats."""

    def get(self):
        describe = Temperature.get_describe()
        chart = Temperature.get_chart()
        context = {
            'describe': describe,
            'chart': chart,
        }
        self.render('share/base.html', **context)
